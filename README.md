# Reddit Client #

A bare bones, minimal reddit client displaying the "hot" feed. 

### Instructions ###

Open Reddit.xcworkspace, build, and run.

### Improvements ###
* Display whole comment tree (have data model, just need to render)
* Ability to choose feed type
* Pull to refresh (feed / comments)
* Auto refresh after interval
* Handle more inline feed media types (GIFs, YouTube, etc.)
* Error handling
* Rich text for comments / post bodies

### Screenshots ###
![Simulator Screen Shot Sep 23, 2016, 4.54.48 PM.png](https://bitbucket.org/repo/8LE79x/images/694511271-Simulator%20Screen%20Shot%20Sep%2023,%202016,%204.54.48%20PM.png) 
![Simulator Screen Shot Sep 23, 2016, 4.55.15 PM.png](https://bitbucket.org/repo/8LE79x/images/2826968604-Simulator%20Screen%20Shot%20Sep%2023,%202016,%204.55.15%20PM.png)