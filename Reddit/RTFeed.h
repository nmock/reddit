//
//  RTFeed.h
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTConstants.h"
#import "RTPost.h"

@interface RTFeed : NSObject

@property (nonatomic, strong) NSArray *posts;

- (void)getPosts:(RTBlock)success failure:(RTErrorBlock)failure;
- (void)getMorePosts:(RTBlock)success failure:(RTErrorBlock)failure;
@end
