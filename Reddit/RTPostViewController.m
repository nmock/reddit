//
//  RTPostViewController.m
//  Reddit
//
//  Created by Nathan Mock on 9/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "RTPostViewController.h"
#import <PureLayout/PureLayout.h>
#import <AMScrollingNavbar/AMScrollingNavbar.h>
#import "RTPostTableViewCell.h"
#import "RTCommentTableViewCell.h"


typedef enum {
    RTSectionTypePostDetails = 0,
    RTSectionTypeComments,
    RTSectionCount
} RTSectionType;

@interface RTPostViewController ()
@property (nonatomic, strong) RTPost *post;
@property (nonatomic, assign) BOOL didSetupConstraints;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicatorView;
@end

@implementation RTPostViewController

- (instancetype)initWithPost:(RTPost*)post {
    self = [self init];
    if (self) {
        self.post = post;
        
        self.tableView = [[UITableView alloc] initForAutoLayout];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self.tableView setNeedsUpdateConstraints];
    
    [self.tableView registerClass:[RTPostTableViewCell class] forCellReuseIdentifier:@"RTPostTableViewCell"];
    [self.tableView registerClass:[RTCommentTableViewCell class] forCellReuseIdentifier:@"RTCommentTableViewCell"];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self showLoading];
    [self.post getComments:^{
        [self hideLoading];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [self hideLoading];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reddit_nav_logo"]];
    self.navigationItem.titleView = logoView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateViewConstraints {
    if (!self.didSetupConstraints) {
        [self.tableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0) excludingEdge:ALEdgeTop];
        NSLayoutConstraint *topLayoutConstraint = [self.tableView autoPinToTopLayoutGuideOfViewController:self withInset:0];
        
        [self followScrollView:self.tableView usingTopConstraint:topLayoutConstraint withDelay:60.0];
        [self setShouldScrollWhenContentFits:NO];
        self.didSetupConstraints = YES;
    }
    
    [super updateViewConstraints];
}


#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return RTSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == RTSectionTypePostDetails) {
        return 1;
    }
    else if (section == RTSectionTypeComments) {
        return [self.post.comments count];
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == RTSectionTypePostDetails) {
        return [RTPostTableViewCell heightWithPost:self.post];
    }
    else if (indexPath.section == RTSectionTypeComments) {
        return [RTCommentTableViewCell heightWithComment:self.post.comments[indexPath.row]];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == RTSectionTypePostDetails) {
        RTPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RTPostTableViewCell" forIndexPath:indexPath];
        [cell updateWithPost:self.post];
        
        return cell;
    }
    else if (indexPath.section == RTSectionTypeComments) {
        RTCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RTCommentTableViewCell" forIndexPath:indexPath];
        [cell updateWithComment:self.post.comments[indexPath.row]];
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // http://stackoverflow.com/a/25877725/349238
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    // This enables the user to scroll down the navbar by tapping the status bar.
    [self showNavbar];
    
    return YES;
}

#pragma mark - State
- (UIActivityIndicatorView*)loadingIndicatorView {
    if (_loadingIndicatorView) {
        return _loadingIndicatorView;
    }
    
    _loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _loadingIndicatorView.frame = CGRectMake(0, 0, 40.0, 40.0);
    _loadingIndicatorView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.00];
    [_loadingIndicatorView startAnimating];
    
    return _loadingIndicatorView;
}

- (void)showLoading {
    [self.loadingIndicatorView startAnimating];
    self.tableView.tableFooterView = self.loadingIndicatorView;
}

- (void)hideLoading {
    [self.loadingIndicatorView stopAnimating];
    self.tableView.tableFooterView = [UIView new];
}

@end
