//
//  RTHomeViewController.h
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTHomeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

