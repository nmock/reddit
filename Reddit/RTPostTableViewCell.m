//
//  RTPostTableViewCell.m
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "RTPostTableViewCell.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "UIView+ViewHelpers.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#import <UIImage+ImageWithColor/UIImage+ImageWithColor.h>

@interface RTPostTableViewCell ()
@property (nonatomic, weak) RTPost *post;
@property (nonatomic, strong) TTTAttributedLabel *titleLabel;
@property (nonatomic, strong) TTTAttributedLabel *detailsLabel;
@property (nonatomic, strong) UIImageView *previewImageView;
@end

@implementation RTPostTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.titleLabel = [TTTAttributedLabel new];
        self.titleLabel.opaque = YES;
        self.titleLabel.numberOfLines = 0;
        [self addSubview:self.titleLabel];
        
        self.detailsLabel = [TTTAttributedLabel new];
        self.detailsLabel.opaque = YES;
        self.detailsLabel.numberOfLines = 0;
        [self addSubview:self.detailsLabel];
        
        self.previewImageView = [UIImageView new];
        [self addSubview:self.previewImageView];
        
        [self applyTheme];
    }
    return self;
}

- (void)applyTheme {
    self.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    self.detailsLabel.font = [UIFont systemFontOfSize:12.0];
    self.detailsLabel.textColor = [UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1.00];
}

- (void)updateWithPost:(RTPost*)post {
    self.post = post;
        
    NSInteger contentWidth = [UIView screenSize].width - (kMargin * 2);
    UIColor *placeholderImageColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.00];
    
    self.titleLabel.text = post.title;
    self.titleLabel.size = [self.titleLabel sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
    self.titleLabel.width = contentWidth;
    self.titleLabel.origin = CGPointMake(kMargin, kMargin);
    
    self.detailsLabel.text = [[post description] uppercaseString];
    self.detailsLabel.size = [self.detailsLabel sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
    self.detailsLabel.width = contentWidth;
    self.detailsLabel.origin = CGPointMake(kMargin, self.titleLabel.bottom + kMargin);
    
    if (post.imagePreviewURL && !post.isAnimated) {
        CGSize imageSize = CGSizeMake([UIView screenSize].width, ([UIView screenSize].width / post.imagePreviewWidth) * post.imagePreviewHeight);
        CGPoint imageOrigin = CGPointMake(0, self.detailsLabel.bottom + kMargin);
        
        self.previewImageView.size = imageSize;
        self.previewImageView.origin = imageOrigin;
        [self.previewImageView setImageWithURL:post.imagePreviewURL placeholderImage:[UIImage imageWithColor:placeholderImageColor size:imageSize]];
    }
    else {
        self.previewImageView.size = CGSizeZero;
        self.previewImageView.image = nil;
    }
}

+ (CGFloat)heightWithPost:(RTPost*)post {
    TTTAttributedLabel *label = [TTTAttributedLabel new];
    
    CGFloat height = kMargin; // start height at top margin
    NSInteger contentWidth = [UIView screenSize].width - (kMargin * 2);
    
    label.numberOfLines = 0;
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.text = post.title;
    height += [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)].height + kMargin;
    
    
    label.font = [UIFont systemFontOfSize:12.0];
    
    label.text = [[post description] uppercaseString];
    
    height += [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)].height + kMargin;

    if (post.imagePreviewURL && !post.isAnimated) {
        height += ([UIView screenSize].width / post.imagePreviewWidth) * post.imagePreviewHeight;
    }
    
    return height;
}

@end
