//
//  RTComment.h
//  Reddit
//
//  Created by Nathan Mock on 9/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTComment : NSObject
@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *author;
@property (nonatomic, strong, readonly) NSString *body;
@property (nonatomic, strong, readonly) NSDate *createdDate;
@property (nonatomic, assign, readonly) NSInteger score;
@property (nonatomic, strong, readonly) NSArray *replies;

- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
@end
