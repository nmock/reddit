//
//  RTPost.h
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTConstants.h"
#import "RTComment.h"

@interface RTPost : NSObject

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *fullNameID;
@property (nonatomic, strong, readonly) NSString *subreddit;
@property (nonatomic, strong, readonly) NSString *author;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, assign, readonly) NSInteger numComments;
@property (nonatomic, strong, readonly) NSArray *comments;
@property (nonatomic, strong, readonly) NSDate *createdDate;
@property (nonatomic, assign, readonly) NSInteger score;
@property (nonatomic, strong, readonly) NSURL *imagePreviewURL;
@property (nonatomic, assign, readonly) NSInteger imagePreviewWidth;
@property (nonatomic, assign, readonly) NSInteger imagePreviewHeight;
@property (nonatomic, assign, readonly) BOOL isAnimated;

- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
- (void)getComments:(RTBlock)success failure:(RTErrorBlock)failure;
@end
