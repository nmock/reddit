//
//  AppDelegate.h
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTHomeViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) RTHomeViewController *homeViewController;

@end

