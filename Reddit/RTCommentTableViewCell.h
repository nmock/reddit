//
//  RTCommentTableViewCell.h
//  Reddit
//
//  Created by Nathan Mock on 9/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTComment.h"

@interface RTCommentTableViewCell : UITableViewCell
- (void)updateWithComment:(RTComment*)comment;
+ (CGFloat)heightWithComment:(RTComment*)comment;
@end
