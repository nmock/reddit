//
//  RTFeed.m
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "RTFeed.h"
#import <AFNetworking/AFHTTPSessionManager.h>

static NSString *const feed_url_string = @"https://www.reddit.com/r/all/hot.json";

@interface RTFeed ()

@end

@implementation RTFeed

- (instancetype)init {
    self = [super init];
    
    if (self) {
        
    }
    
    return self;
}



- (void)getPosts:(RTBlock)success failure:(RTErrorBlock)failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:feed_url_string parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        self.posts = [self mapPosts:responseObject];
        
        if (success) {
            success();
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}


- (void)getMorePosts:(RTBlock)success failure:(RTErrorBlock)failure {
    RTPost *lastPost = [self.posts lastObject];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:feed_url_string parameters:@{@"after" : lastPost.fullNameID} progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        
        self.posts = [self mapPosts:responseObject];
        
        if (success) {
            success();
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}

- (NSArray *)mapPosts:(NSDictionary*)responseObject {
    if (responseObject[@"data"] && [responseObject[@"data"][@"children"] isKindOfClass:[NSArray class]] && [((NSArray*)responseObject[@"data"][@"children"]) count] > 0) {
        NSMutableArray *posts = [NSMutableArray arrayWithArray:self.posts];
        
        for (NSDictionary *postDictionary in responseObject[@"data"][@"children"]) {
            if (postDictionary[@"data"]) {
                RTPost *post = [[RTPost alloc] initWithDictionary:postDictionary[@"data"]];
                if (![posts containsObject:post]) {
                    [posts addObject:post];
                }
            }
        }
        
        return posts;
    }
    
    return nil;
}

@end
