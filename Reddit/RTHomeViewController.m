//
//  RTHomeViewController.m
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "RTHomeViewController.h"
#import <PureLayout/PureLayout.h>
#import <AMScrollingNavbar/AMScrollingNavbar.h>
#import "RTFeed.h"
#import "RTPostTableViewCell.h"
#import "RTPostViewController.h"
#import <SKSplashView/SKSplashIcon.h>

@interface RTHomeViewController ()
@property (nonatomic, assign) BOOL didSetupConstraints;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) RTFeed *feed;
@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicatorView;
@property (nonatomic, strong) SKSplashView *splashView;
@end

@implementation RTHomeViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reddit_nav_logo"]];
        self.navigationItem.titleView = logoView;
//        self.title = @"Reddit";
        
        self.tableView = [[UITableView alloc] initForAutoLayout];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        self.feed = [[RTFeed alloc] init];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self.tableView setNeedsUpdateConstraints];
    
    [self.tableView registerClass:[RTPostTableViewCell class] forCellReuseIdentifier:@"RTPostTableViewCell"];
    
    self.navigationItem.hidesBackButton = YES;
    
    [self redditSplash];
    
    [self showLoading];
    [self.feed getPosts:^{
        [self hideLoading];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [self hideLoading];
    }];
}

- (void)updateViewConstraints {
    if (!self.didSetupConstraints) {
        [self.tableView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0) excludingEdge:ALEdgeTop];
        NSLayoutConstraint *topLayoutConstraint = [self.tableView autoPinToTopLayoutGuideOfViewController:self withInset:0];
        
        [self followScrollView:self.tableView usingTopConstraint:topLayoutConstraint withDelay:60.0];
        [self setShouldScrollWhenContentFits:NO];
        self.didSetupConstraints = YES;
    }
    
    [super updateViewConstraints];
}

- (void)redditSplash {
    SKSplashIcon *redditSplashIcon = [[SKSplashIcon alloc] initWithImage:[UIImage imageNamed:@"reddit_ghost_logo_mini"] animationType:SKIconAnimationTypeGrow];
    UIColor *redditColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
    _splashView = [[SKSplashView alloc] initWithSplashIcon:redditSplashIcon backgroundColor:redditColor animationType:SKSplashAnimationTypeFade];
    _splashView.animationDuration = 1.2;
    [self.view addSubview:_splashView];
    [_splashView startAnimation];
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    // This enables the user to scroll down the navbar by tapping the status bar.
    [self showNavbar];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.feed.posts count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    RTPost *post = self.feed.posts[indexPath.row];
    return [RTPostTableViewCell heightWithPost:post];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RTPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RTPostTableViewCell" forIndexPath:indexPath];
    
    RTPost *post = self.feed.posts[indexPath.row];
    [cell updateWithPost:post];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // http://stackoverflow.com/a/25877725/349238
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (indexPath.row > [self.feed.posts count] - 4) {
        [self showLoading];
        [self.feed getMorePosts:^{
            [self hideLoading];
            [self.tableView reloadData];
        } failure:^(NSError *error) {
            [self hideLoading];
        }];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self showNavbar];
    
    RTPost *post = self.feed.posts[indexPath.row];
    RTPostViewController *postVC = [[RTPostViewController alloc] initWithPost:post];
    [self.navigationController pushViewController:postVC animated:YES];
}

#pragma mark - State
- (UIActivityIndicatorView*)loadingIndicatorView {
    if (_loadingIndicatorView) {
        return _loadingIndicatorView;
    }
    
    _loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _loadingIndicatorView.frame = CGRectMake(0, 0, 40.0, 40.0);
    _loadingIndicatorView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.00];
    [_loadingIndicatorView startAnimating];
    
    return _loadingIndicatorView;
}

- (void)showLoading {
    [self.loadingIndicatorView startAnimating];
    self.tableView.tableFooterView = self.loadingIndicatorView;
}

- (void)hideLoading {
    [self.loadingIndicatorView stopAnimating];
    self.tableView.tableFooterView = [UIView new];
}

@end
