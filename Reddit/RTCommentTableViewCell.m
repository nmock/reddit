//
//  RTCommentTableViewCell.m
//  Reddit
//
//  Created by Nathan Mock on 9/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "RTCommentTableViewCell.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "UIView+ViewHelpers.h"

@interface RTCommentTableViewCell ()
@property (nonatomic, weak) RTComment *comment;
@property (nonatomic, strong) TTTAttributedLabel *bodyLabel;
@property (nonatomic, strong) TTTAttributedLabel *detailsLabel;
@property (nonatomic, strong) UIImageView *previewImageView;
@end

@implementation RTCommentTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.bodyLabel = [TTTAttributedLabel new];
        self.bodyLabel.opaque = YES;
        self.bodyLabel.numberOfLines = 0;
        [self addSubview:self.bodyLabel];
        
        self.detailsLabel = [TTTAttributedLabel new];
        self.detailsLabel.opaque = YES;
        self.detailsLabel.numberOfLines = 0;
        [self addSubview:self.detailsLabel];
        
        [self applyTheme];
    }
    return self;
}

- (void)applyTheme {
    self.bodyLabel.font = [UIFont systemFontOfSize:14.0];
    self.detailsLabel.font = [UIFont systemFontOfSize:12.0];
    self.detailsLabel.textColor = [UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1.00];
}

- (void)updateWithComment:(RTComment*)comment {
    self.comment = comment;
    
    NSInteger contentWidth = [UIView screenSize].width - (kMargin * 2);
    
    self.bodyLabel.text = comment.body;
    self.bodyLabel.size = [self.bodyLabel sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
    self.bodyLabel.width = contentWidth;
    self.bodyLabel.origin = CGPointMake(kMargin, kMargin);
    
    self.detailsLabel.text = [[comment description] uppercaseString];
    self.detailsLabel.size = [self.detailsLabel sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
    self.detailsLabel.width = contentWidth;
    self.detailsLabel.origin = CGPointMake(kMargin, self.bodyLabel.bottom + kMargin);
}

+ (CGFloat)heightWithComment:(RTComment*)comment {
    TTTAttributedLabel *label = [TTTAttributedLabel new];
    
    CGFloat height = kMargin; // start height at top margin
    NSInteger contentWidth = [UIView screenSize].width - (kMargin * 2);
    
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:14.0];
    label.text = comment.body;
    height += [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)].height + kMargin;
    
    
    label.font = [UIFont systemFontOfSize:12.0];
    
    label.text = [[comment description] uppercaseString];
    
    height += [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)].height + kMargin;
    
    return height;
}

@end
