//
//  RTConstants.h
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#ifndef RTConstants_h
#define RTConstants_h

typedef void (^RTBlock)(void);
typedef void (^RTErrorBlock)(NSError *error);

#endif /* RTConstants_h */
