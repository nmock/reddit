//
//  RTPostViewController.h
//  Reddit
//
//  Created by Nathan Mock on 9/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTPost.h"

@interface RTPostViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
- (instancetype)initWithPost:(RTPost*)post;
@end
