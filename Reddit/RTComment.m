//
//  RTComment.m
//  Reddit
//
//  Created by Nathan Mock on 9/23/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "RTComment.h"
#import <DateTools/NSDate+DateTools.h>

static NSString *const key_id                           = @"id";
static NSString *const key_author                       = @"author";
static NSString *const key_body                         = @"body";
static NSString *const key_created_date                 = @"created";
static NSString *const key_score                        = @"score";
static NSString *const key_replies                      = @"replies";

@implementation RTComment
- (instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    
    if (self) {
        [self updateValuesFromDictionary:dictionary];
    }
    
    return self;
}

- (void)updateValuesFromDictionary:(NSDictionary*)dictionary {
    if (dictionary[key_id]) {
        _identifier = dictionary[key_id];
    }

    if (dictionary[key_author]) {
        _author = dictionary[key_author];
    }
    
    if (dictionary[key_created_date]) {
        NSTimeInterval epochSeconds = [dictionary[key_created_date] doubleValue];
        _createdDate = [NSDate dateWithTimeIntervalSince1970:epochSeconds];
    }
    
    if (dictionary[key_score]) {
        _score = [dictionary[key_score] integerValue];
    }
    
    if (dictionary[key_body]) {
        _body = dictionary[key_body];
    }
    
    if (dictionary[key_replies] && [dictionary[key_replies] isKindOfClass:[NSDictionary class]]) {
        NSMutableArray *replies = [NSMutableArray new];
        
        for (NSDictionary *replyCommentDictionary in dictionary[key_replies][@"data"][@"children"]) {
            RTComment *reply = [[RTComment alloc] initWithDictionary:replyCommentDictionary[@"data"]];
            if (![replies containsObject:reply]) {
                [replies addObject:reply];
            }
        }
        
        _replies = replies;
    }
}

- (NSString*)description {
    return [NSString stringWithFormat:@"%@ · %@", self.author, [self.createdDate timeAgoSinceNow]];
}

- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[RTComment class]]) {
        return NO;
    }
    
    RTComment *comment = object;
    return self == comment || [self.identifier isEqualToString:comment.identifier];
}

@end
