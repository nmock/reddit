//
//  RTPost.m
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import "RTPost.h"
#import <DateTools/NSDate+DateTools.h>
#import <AFNetworking/AFHTTPSessionManager.h>

@implementation RTPost

static NSString *const key_id                           = @"id";
static NSString *const key_full_name_id                 = @"name";
static NSString *const key_subreddit                    = @"subreddit";
static NSString *const key_author                       = @"author";
static NSString *const key_title                        = @"title";
static NSString *const key_created_date                 = @"created";
static NSString *const key_score                        = @"score";
static NSString *const key_num_comments                 = @"num_comments";
static NSString *const key_num_image_preview            = @"preview";

- (instancetype)initWithDictionary:(NSDictionary*)dictionary {
    self = [super init];
    
    if (self) {
        [self updateValuesFromDictionary:dictionary];
    }
    
    return self;
}

- (void)updateValuesFromDictionary:(NSDictionary*)dictionary {
    if (dictionary[key_id]) {
        _identifier = dictionary[key_id];
    }
    
    if (dictionary[key_full_name_id]) {
        _fullNameID = dictionary[key_full_name_id];
    }
    
    if (dictionary[key_subreddit]) {
        _subreddit = dictionary[key_subreddit];
    }
    
    if (dictionary[key_author]) {
        _author = dictionary[key_author];
    }
    
    if (dictionary[key_title]) {
        _title = dictionary[key_title];
    }
    
    if (dictionary[key_num_comments]) {
        _numComments = [dictionary[key_num_comments] integerValue];
    }
    
    if (dictionary[key_created_date]) {
        NSTimeInterval epochSeconds = [dictionary[key_created_date] doubleValue];
        _createdDate = [NSDate dateWithTimeIntervalSince1970:epochSeconds];
    }
    
    if (dictionary[key_score]) {
        _score = [dictionary[key_score] integerValue];
    }
    
    if (dictionary[key_author]) {
        _author = dictionary[key_author];
    }
    
    if (dictionary[key_num_image_preview]) {
        NSDictionary *sourceImageDictionary = dictionary[key_num_image_preview][@"images"][0][@"source"];
        _imagePreviewURL = [NSURL URLWithString:sourceImageDictionary[@"url"]];
        _imagePreviewWidth = [sourceImageDictionary[@"width"] integerValue];
        _imagePreviewHeight = [sourceImageDictionary[@"height"] integerValue];
        
        if (dictionary[key_num_image_preview][@"images"][0][@"variants"][@"gif"]) {
            _isAnimated = YES;
            _imagePreviewURL = [NSURL URLWithString:dictionary[key_num_image_preview][@"images"][0][@"variants"][@"gif"][@"source"][@"url"]];
        }
    }
}

- (NSString*)commentsURLString {
    return [NSString stringWithFormat:@"https://www.reddit.com/r/%@/comments/%@.json", self.subreddit, self.identifier];
}

- (void)getComments:(RTBlock)success failure:(RTErrorBlock)failure {
    if (!self.fullNameID) {
        // call failure block
        return;
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:[self commentsURLString] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);

        _comments = [self mapComments:responseObject];
        
        if (success) {
            success();
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            failure(error);
        }
    }];
}

- (NSArray *)mapComments:(NSArray*)responseObject {
    [self updateValuesFromDictionary:responseObject[0]];
    
    NSMutableArray *comments = [NSMutableArray arrayWithArray:self.comments];
    
    for (NSDictionary *commentDictionary in responseObject[1][@"data"][@"children"]) {
        RTComment *comment = [[RTComment alloc] initWithDictionary:commentDictionary[@"data"]];
        if (comment.author && ![comments containsObject:comment]) {
            [comments addObject:comment];
        }
    }
    
    return comments;
}


- (NSString*)description {
    return [NSString stringWithFormat:@"%ld comments · %@ · %@", self.numComments, [self.createdDate timeAgoSinceNow], self.subreddit];
}

- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[RTPost class]]) {
        return NO;
    }
    
    RTPost *post = object;
    return self == post || [self.fullNameID isEqualToString:post.fullNameID];
}
@end
