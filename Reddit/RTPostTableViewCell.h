//
//  RTPostTableViewCell.h
//  Reddit
//
//  Created by Nathan Mock on 9/22/16.
//  Copyright © 2016 Nathan Mock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTPost.h"

@interface RTPostTableViewCell : UITableViewCell
- (void)updateWithPost:(RTPost*)post;
+ (CGFloat)heightWithPost:(RTPost*)post;
@end
